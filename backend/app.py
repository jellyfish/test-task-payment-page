import time

from flask import Flask, jsonify, request
from flask_cors import CORS

from models.product import ProductScannerModel
from models.account import AccountConnectorModel

app = Flask(__name__)
CORS(app)


def build_response(data, success=True, status='OK'):

    return jsonify({
            'data': data,
            'success': success,
            'status': status
            })


@app.route('/')
def main():
    return build_response({})


@app.route('/api/product-scanner/action/check', methods=['POST'])
def check_product_scanner():

    data = request.get_json()

    model = ProductScannerModel(**data)

    if model.valid():
        return build_response('No errors')
    else:
        return build_response(model.errors, False, 'BAD_REQUEST'), 400


@app.route('/api/account-connector/action/check', methods=['POST'])
def check_account_connector():

    data = request.get_json()

    model = AccountConnectorModel(**data)

    if model.valid():
        return build_response('No errors')
    else:
        return build_response(model.errors, False, 'BAD_REQUEST'), 400


@app.route('/api/plan', methods=['PUT'])
def update_plan():

    time.sleep(2)

    return build_response("No errors")
