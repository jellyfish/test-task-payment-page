from models.model import Model


class ProductScannerModel(Model):

    """ Model which represents data layer for Product scanner tool
    """

    rules = {
            'number': [(
                'more_than_two_hundred',
                'TO_FEW_NUMBER',
                'Number is less than count of the products'
                )],

            'crawls': [(
                'more_than_zero',
                'LESS_THAN_ZERO',
                'Crawls is less than zero'
                )]
            }

    def more_than_two_hundred(self, number):
        return number > 200

    def more_than_zero(self, number):
        return number > 0
