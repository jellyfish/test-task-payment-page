from models.model import Model


class AccountConnectorModel(Model):

    """ Model which represents data layer for Account Connector tool
    """
