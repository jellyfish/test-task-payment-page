

class Model(object):

    """ Abstract Model class
    """

    rules = {}

    def __init__(self, **data):
        self.errors = {}

        for key, value in data.items():
            setattr(self, key, value)

    def valid(self):
        """valid
            checking is the properties valid according to rules config:

            rules config format:

            {
                property: [(validation_method, error_short_code,
                error_description)]
                ...
            }
        """

        for _property, value in self.__dict__.items():

            if _property not in self.rules:
                continue

            for rule in self.rules.get(_property):

                if not isinstance(rule, tuple):
                    raise Exception('Invalid validation rules')

                _callable = getattr(self, rule[0])

                success = _callable(getattr(self, _property))

                if not success:
                    self.errors.update({
                        _property: (rule[1], rule[2])
                        })

        return not len(self.errors.keys()) > 0
