#!/bin/bash

cd backend
virtualenv env

source ./env/bin/activate
pip install -r requirements.txt

cd ../frontend
npm install
