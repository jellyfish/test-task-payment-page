import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from './app.service';

declare var Slider:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  productScannerSlider:number = 0;
  crowlScannerSlider: number = 0;
  accountScannerSlider: number = 0;
  public slider:any = null;
  private _productScannerMapping = {
    0: [0, 0],
    25: [100, 50],
    50: [200, 75],
    75: [500, 150],
    100: [1000, 250]
  }
  public sliderCrowl:any = null;
  private _crowlScannerMapping = {
    0: [1, 1],
    25: [2, 2],
    50: [4, 3],
    75: [8, 5],
    100: [30, 15]
  }
  public sliderAccount:any = null;
  private _accountScannerMapping = {
    0: [0, 0],
    25: [1, 10],
    50: [2, 17.50],
    75: [5, 30],
    100: [10, 50]
  }

  product: number = 0;
  crowl: number = 1;
  account: number = 0;
  sum: any = 0;
  updBtn: Element;
  flag: boolean = true;
  error: boolean = false;

  constructor(private _as: AppService) {
  }

  ngOnInit(){
    this.slider = new Slider('input.slider-product-scanner', {
      min: 0,
      max: 100,
      value: 0,
      ticks: [0, 25, 50, 75, 100],
      ticks_labels: ["Off($0)", "100($50)", "200($75)", "500($150)", "1000($250)"],
      ticks_snap_bounds: 25
    })

    this.sliderCrowl = new Slider('input.slider-crowl-scanner', {
      min: 0,
      max: 100,
      value: 0,
      ticks: [0, 25, 50, 75, 100],
      ticks_labels: ["1", "2 (2x)", "4 (3x)", "8 (5x)", "30(15x)"],
      ticks_snap_bounds: 25
    })

    this.sliderAccount = new Slider('input.slider-account-scanner', {
      min: 0,
      max: 100,
      value: 0,
      ticks: [0, 25, 50, 75, 100],
      ticks_labels: ["Off($0)", "1($10)", "2($17.50)", "5($30)", "10($50)"],
      ticks_snap_bounds: 25
    })

    this.updBtn = document.getElementById('update')
    this.setValues()

    document.getElementById('center').style.opacity = '1';
  }

  setValues(): void {
    this.productScannerSlider = this._productScannerMapping[this.slider.element.value][0];
    this.crowlScannerSlider = this._crowlScannerMapping[this.sliderCrowl.element.value][0];
    this.accountScannerSlider = this._accountScannerMapping[this.sliderAccount.element.value][0];
    this.slider.setValue(+this.slider.element.value)
    this.sliderCrowl.setValue(+this.sliderCrowl.element.value)
    this.sliderAccount.setValue(+this.sliderAccount.element.value)
    this._as.checkProduct(this.productScannerSlider, this.crowlScannerSlider)
      .subscribe(
        data=>{
          this.product = this._productScannerMapping[this.slider.element.value][1];
          this.crowl = this._crowlScannerMapping[this.sliderCrowl.element.value][1];
          this.account = this._accountScannerMapping[this.sliderAccount.element.value][1];
          this.sum = this.product * this.crowl + this.account;
          this.flag = false
          this.error = false
          console.log(data)
        },
        error=>{
          this.product = this._productScannerMapping[this.slider.element.value][1];
          this.crowl = this._crowlScannerMapping[this.sliderCrowl.element.value][1];
          this.account = this._accountScannerMapping[this.sliderAccount.element.value][1];
          this.sum = this.product * this.crowl + this.account;         
          this.error = true
          this.flag = true
          console.error(error)
        }
      )
    this._as.checkAccount(this.accountScannerSlider)
      .subscribe(
        data=>{
          console.log(data)
        },
        error=>{
          console.error(error)
          this.flag = true
        }
      )
  }

  updatePlan(): void{
    this.flag = true
    this._as.checkProduct(this.productScannerSlider, this.crowlScannerSlider)
      .subscribe(
        data=>{
          this._as.updatePlan()
            .subscribe(
              data => {
                this.flag = false
                console.log(data)
              },
              error => {
                console.error(error)
              }
            )
          console.log(data)
        },
        error=>{
          console.error(error)
        }
      )
  }
}
