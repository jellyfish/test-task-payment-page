import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AppService {
    baseUrl: string;
    constructor(private _http: Http) {
      this.baseUrl = environment.server;
     }

    checkProduct(product: number, crowl: number): Observable<object>{
      let body = {
        "crowl": crowl,
        "number":product
      }
      const url = `${this.baseUrl}/api/product-scanner/action/check`;
      return this._http
      .post(url, body,{ headers: new Headers({ 'Access-Control-Allow-Origin': '*'  }) })
      .map(this.extractData)
      .catch(this._errorHandler);
    }

     checkAccount(acc: number): Observable<object>{
      const url = `${this.baseUrl}/api/account-connector/action/check`;
      return this._http
      .post(url, { acc },{ headers: new Headers({ 'Access-Control-Allow-Origin': '*'  }) })
      .map(this.extractData)
      .catch(this._errorHandler);
    }

    updatePlan(): Observable<object>{
      const url = `${this.baseUrl}/api/plan`;
      return this._http
      .put(url, {},{ headers: new Headers({ 'Access-Control-Allow-Origin': '*'  }) })
      .map(this.extractData)
      .catch(this._errorHandler);
    }

      extractData(res: Response) {
        const data = res.json();
        return data || {};
      }

      private _errorHandler(err: Response | any) {
        let errMsg: string;

        if (err instanceof Response) {
          const body = err.json();
          const error = body.error || JSON.stringify(body);
          errMsg = `${err.status} - ${err.statusText || ''} ${error}`;
        } else {
          errMsg = err.message ? err.message : err.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
      }
}
