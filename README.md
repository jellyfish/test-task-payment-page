# Requirements
1. `virtualenv` ([installation instructions](https://github.com/angular/angular-cli#installation))
2. `@angular/cli` ([installation instructions](https://github.com/angular/angular-cli#installation))

# Steps
1. Run `install.sh`
2. In first terminal run `run-frontend.sh`
3. In second terminal run `run-backend.sh`
